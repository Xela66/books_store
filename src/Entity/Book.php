<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use App\Repository\BookRepository;

/**
 * @UniqueEntity("title")
 * @UniqueEntity("isbn")
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     min="4",
     *     minMessage="Il n'y a pas assez de character"
     * )
     *
     */
    private ?string $title;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $synopsis;

    /**
     * @ORM\Column(type="date")
     */
    private ?\DateTimeInterface $dateParution;

    /**
     * @ORM\Column(type="date")
     */
    private ?\DateTimeInterface $dateEdition;

    /**
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=true,
     *     message="Veuillez entrer un chiffre"
     * )
     * @ORM\Column(type="integer")
     */
    private ?int $nbrPage;

    /**
     *
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false
     * )
     * @ORM\Column(type="string", length=255)
     */
    private ?string $editor;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private ?string $isbn;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private ?string $ean;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="books")
     */
    private Collection $author;

    /**
     * @ORM\ManyToOne(targetEntity=Genre::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Genre $genre;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class)
     */
    private Collection $category;

    public function __construct()
    {
        $this->author = new ArrayCollection();
        $this->category = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getDateParution(): ?\DateTimeInterface
    {
        return $this->dateParution;
    }

    public function setDateParution(\DateTimeInterface $dateParution): self
    {
        $this->dateParution = $dateParution;

        return $this;
    }

    public function getDateEdition(): ?\DateTimeInterface
    {
        return $this->dateEdition;
    }

    public function setDateEdition(\DateTimeInterface $dateEdition): self
    {
        $this->dateEdition = $dateEdition;

        return $this;
    }

    public function getNbrPage(): ?int
    {
        return $this->nbrPage;
    }

    public function setNbrPage(int $nbrPage): self
    {
        $this->nbrPage = $nbrPage;

        return $this;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function setEditor(string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getEan(): ?string
    {
        return $this->ean;
    }

    public function setEan(string $ean): self
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * @return Collection<int, author>
     */
    public function getAuthor(): Collection
    {
        return $this->author;
    }

    public function addAuthor(author $author): self
    {
        if (!$this->author->contains($author)) {
            $this->author[] = $author;
        }

        return $this;
    }

    public function removeAuthor(author $author): self
    {
        $this->author->removeElement($author);

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->category->removeElement($category);

        return $this;
    }

    public function getCategoriesNames(): ?string
    {
        $res = "";
        for ($i = 0; $i < count($this->getCategory()); $i++) {
            $res .= $this->getCategory()[$i]->getLabel();
            $res .= ($i < count($this->getCategory()) - 1) ? (', ') : ('');
        }
        return $res;
    }
}
