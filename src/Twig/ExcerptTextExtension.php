<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ExcerptTextExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/3.x/advanced.html#automatic-escaping
            new TwigFilter('excerpt', [$this, 'formatExcerpt']),
        ];
    }

    public function formatExcerpt($text, $maxlen = 200, $elipsis = "...")
    {
        if (strlen($text) <= $maxlen) {
            return $text;
        }

        return substr($text, 0, $maxlen - strlen($elipsis)) . $elipsis;
    }
}
