<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FormatLetterExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/3.x/advanced.html#automatic-escaping
            new TwigFilter('formatLetter', [$this, 'formatLetter'],
                ['is_safe' => ['html']]),
        ];
    }

    public function formatLetter($text, $letter = 'n')
    {
        $pattern = "/" . $letter . "/"; // pattern de recherche ce la lettre /n/
        $text = preg_replace($pattern,
            "<strong>" . mb_strtoupper($letter) . "</strong>", $text);
        return $text;
    }
}
