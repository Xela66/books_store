<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\Author;

class AuthorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastName', null, [
                'label' => "Nom de l'auteur",
                'attr' => ["placeholder" => "nom"],
                'required' => true
            ])
            ->add('firstName', null, [
                'label' => "Prénom de l'auteur",
                'required' => true,
                'attr' => ["placeholder" => "prénom"],
            ])
            ->add('birthdate', DateType::class, [
                "widget" => "single_text",
                'format' => 'yyyy-MM-dd',
                'attr' => ["class" => "form-control"],
                'label' => "Date de naissance"
            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => "Avatar de l'auteur",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Author::class,
        ]);
    }
}
