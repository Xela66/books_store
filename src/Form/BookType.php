<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\Genre;
use App\Repository\AuthorRepository;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, ["label" => "Titre :"])
            ->add('synopsis', null, ["label" => "Synopsis :"])
            ->add('dateParution', null, ["label" => "Date de parution :"])
            ->add('dateEdition', null, ["label" => "Date d'édition :"])
            ->add('nbrPage', TextType::class, ["label" => "Nombre de page :"])
            ->add('editor', null, ["label" => "Editeur :"])
            ->add('isbn', null, ["label" => "Isbn :"])
            ->add('ean', null, ["label" => "Ean :"])
            ->add('author', EntityType::class, [
                'label' => "Nom de l'auteur :",
                'query_builder' => function (AuthorRepository $authorRepo) {
                    return $authorRepo->orderLabel();
                },
                'class' => Author::class,
                'placeholder' => "Choisir un auteur",
                'choice_label' => "lastName",
                'mapped' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('genre', EntityType::class, [
                'label' => "Genre :",
                'class' => Genre::class,
                'placeholder' => "Choisir un genre",
                'choice_label' => "label",
                'mapped' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('category', EntityType::class, [
                'label' => "Catégorie :",
                'class' => Category::class,
                'placeholder' => "Choisir un type",
                'choice_label' => "label",
                'mapped' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('submit', SubmitType::class, ['label' => 'Sauvegarder']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
