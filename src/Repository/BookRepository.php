<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use App\Entity\Book;

/**
 * @extends ServiceEntityRepository<Book>
 *
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function add(Book $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Book $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByCreationUnder($date): array
    {
        $db = $this->findAllOptimise();

        return $db->where('b.dateParution < :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    public function findLatest()
    {
        return $this->createQueryBuilder('b')
            ->orderBy("b.dateParution", "DESC")
            ->setMaxResults(5)
            ->getQuery()->getResult();
    }

    public function findAllBooks()
    {
        $db = $this->findAllOptimise();

        return $db->getQuery()->getResult();
    }

    private function findAllOptimise()
    {
        return $this->createQueryBuilder('b')
            ->leftJoin("b.author", "a")
            ->addSelect('a')
            ->leftJoin('b.genre', 'g')
            ->addSelect('g')
            ->leftJoin("b.category", 'c')
            ->addSelect('c')
            ->orderBy("b.dateEdition", "DESC");
    }
}
