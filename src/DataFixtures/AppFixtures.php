<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\Genre;
use App\Entity\User;

class AppFixtures extends Fixture
{
    /**
     * @var
     */
    private UserPasswordHasherInterface $pass_hasher;

    /**
     * @param UserPasswordHasherInterface $passwordHasher
     */
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->pass_hasher = $passwordHasher;
    }

    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $genres = ['Polar', 'Roman', 'Science-Fiction', 'Jeunesse', 'Actualités', 'Biographie'];
        $categories = ['Poche', 'Broché', 'Bande-dessinée', 'Mange', 'Epub'];

        $faker = Faker\Factory::create('fr_FR');

        $userOne = new User();
        $userOne->setFirstName("Collado")
            ->setLastName("Alexandre")
            ->setEmail("alexandre@admin.com")
            ->setRole('ROLE_ADMIN')
            ->setPassword($this->pass_hasher->hashPassword($userOne, "admin"));
        $manager->persist($userOne);
        $userTwo = new User();
        $userTwo->setFirstName("Doe")
            ->setLastName("John")
            ->setEmail("utilisateur@user.com")
            ->setRole('ROLE_USER')
            ->setPassword($this->pass_hasher->hashPassword($userTwo, "user"));
        $manager->persist($userTwo);
        foreach ($genres as $kg => $genre) { // traitement du genre
            $gn = new Genre();
            $gn->setLabel($genre);
            $manager->persist($gn);
            $this->addReference('genre-' . $kg, $gn);
        }
        foreach ($categories as $kc => $category) { // traitement des catégories
            $cat = new Category();
            $isornot = ($category == 'Epub') ? (true) : (false);
            $cat->setLabel($category)->setElectronic($isornot);
            $manager->persist($cat);
            $this->addReference('cat-' . $kc, $cat);
        }
        for ($i = 0; $i < 15; $i++) { // génération de 15 auteurs
            $author = new Author();
            $author->setLastName($faker->lastName)
                ->setFirstName($faker->firstName)
                ->setFilename('default.jpg')
                ->setUpdateAt($faker->dateTime('now'))
                ->setBirthdate($faker->dateTime('2004-01-01'));
            $manager->persist($author);
            // contruire le tableau de référence
            $this->addReference('author-' . $i, $author);
        }
        for ($i = 0; $i < 30; $i++) {  // generation de 30 livres
            $book = new Book();
            $book->setTitle($faker->sentence(6, true))
                ->setGenre($this->getReference('genre-' . rand(0, count($genres) - 1)))
                ->setDateParution($faker->dateTime('2000-01-01'))
                ->setDateEdition($faker->dateTime('now'))
                ->setNbrPage($faker->randomNumber(3))
                ->setSynopsis($faker->paragraph(5, true))
                ->setEan($faker->ean13)
                ->setIsbn($faker->isbn13)
                ->setEditor($faker->company);
            $maxCat = rand(1, count($categories)); // ajout des catégories
            for ($c = 1; $c < $maxCat; $c++) {
                $book->addCategory($this->getReference('cat-' . rand(0, count($categories) - 1)));
            }
            $maxAuthor = rand(1, 5); // ajouté les référence d'auteurs
            for ($u = 1; $u <= $maxAuthor; $u++) {
                // ajout de l'auteur par sa référence
                $book->addAuthor($this->getReference('author-' . rand(0, 14)));
            }
            $manager->persist($book);
        }
        $manager->flush();
    }
}
