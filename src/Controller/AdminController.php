<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/home", name="dashboard", methods={"GET"})
     *
     */
    public function homeAdmin(): Response
    {
        return $this->render("admin/dashboard.html.twig", []);
    }

    /**
     * @Route("/logout", name="logout")
     * @return void
     */
    public function logout()
    {
        $this->redirectToRoute("app_home");
    }
}