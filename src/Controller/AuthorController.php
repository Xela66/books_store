<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Author;
use App\Form\AuthorType;
use App\Repository\AuthorRepository;

/**
 * @Route("/admin")
 */
class AuthorController extends AbstractController
{
    private AuthorRepository $authorRepo;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepo = $authorRepository;
    }

    /**
     * @Route("/author", name="app_author")
     */
    public function listAuthor(): Response
    {
        $authors = $this->authorRepo->findAll();
        return $this->render("author/listAuthor.html.twig", [
            "authors" => $authors,
        ]);
    }

    /**
     * @Route("/addAuthor/{id}", name="newAuthor", methods={"GET","POST"}, requirements={"id": "\d+"})
     * @param int $id
     * @param Request $request
     * @param ManagerRegistry $manager
     * @return Response
     */
    public function addAuthor(int $id = -1, Request $request, ManagerRegistry $manager): Response
    {

        $author = ($id > 0) ? ($this->authorRepo->find($id)) : (new Author()); // instanciation d'un objet vide

        $formulaire = $this->createForm(AuthorType::class, $author); // création du formulaire et association de l'instance vide
        $formulaire->handleRequest($request); // surveillance du request

        if ($formulaire->isSubmitted() && $formulaire->isValid()) { // si request post et form valide

            $em = $manager->getManager();
            $em->persist($author);
            $em->flush();

            $this->addFlash('success', "L'auteur a été crée");

            return $this->redirectToRoute("app_author"); // redirection vers la liste
        }
        return $this->render("author/editAuthor.html.twig", [
            "form" => $formulaire->createView()
        ]);
    }

    /**
     * @Route("/delAut/{id}", name="delAuteur", methods={"POST"}, requirements={"id": "\d+"})
     * @param int $id
     * @param Request $request
     * @param ManagerRegistry $managerRegistry
     * @return Response
     */
    public function deleteAuthor(int $id, Request $request, ManagerRegistry $managerRegistry): Response
    {
        if ($this->isCsrfTokenValid('delete' . $id, $request->get('_token'))) {
            $em = $managerRegistry->getManager();
            $author = $this->authorRepo->find($id);
            $em->remove($author);
            $em->flush();
            $this->addFlash('success', "L'auteur a été supprimé !");
            return $this->redirectToRoute("app_author");
        } else {
            return new Response("<h1>Identifiant invalide</h1>");
        }
    }
}