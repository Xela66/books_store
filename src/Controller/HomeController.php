<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;

class HomeController extends AbstractController
{
    /**
     * @var BookRepository
     */
    private BookRepository $bookRepo;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepo = $bookRepository;
    }

    /**
     * @Route("/", name="app_home", methods={"GET"})
     * @return Response
     */
    public function bienvenue(): Response
    {
        return $this->render("front/home.html.twig", []);
    }

    /**
     * @Route("/page/{numPage}", name="app_page", methods={"GET"}, )
     * @param string $numPage
     * @return Response
     */
    public function page(string $numPage): Response
    {
        return $this->render("front/page.html.twig", [
            "numPage" => $numPage,
        ]);
    }

    /**
     * @Route("/listeArticles", name="app_list", methods={"GET"})
     */
    public function listArticles(): Response
    {
        $articles = [
            ["title" => "Les lois de JAVA", "comment" => "Tout savoir sur le JAVA"],
            ["title" => "La beauté du C", "comment" => "Ecrire le C avec élégance"],
            ["title" => "Mon wordpress adoré", "comment" => "Une relation spécial aux Gabriel(s)"],
        ];

        return $this->render("front/template_part/_listeArticles.html.twig", [
            "articles" => $articles,
        ]);
    }

    /**
     * @Route("/books", name="app_books", methods={"GET"})
     * @return Response
     */
    public function bookList(): Response
    {
        $books = $this->bookRepo->findAllBooks();

        return $this->render("front/books.html.twig", [
            "books" => $books
        ]);
    }

    /**
     * @Route("/bookDateFilter", name="dateFiltered", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function listBookFilter(Request $request): Response
    {
        $datePost = $request->get('date');
        $books = $this->bookRepo->findByCreationUnder($datePost);
        return $this->render("front/booksFiltered.html.twig", [
            "books" => $books
        ]);
    }

    /**
     * @Route("/latestBooks", name="latest", methods={"GET"})
     * @return Response
     */
    public function latestBooks(): Response
    {
        $books = $this->bookRepo->findLatest();

        return $this->render('front/latestBooks.html.twig', [
            'books' => $books,
        ]);
    }

    /**
     * @Route("/addbook", name="addBook", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function addBook(Request $request): Response
    {
        $book = new  Book();

        $b_form = $this->createForm(BookType::class, $book);

        return $this->render("front/form/bookForm.html.twig", [
            "form" => $b_form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastIdent = $authenticationUtils->getLastUsername();

        return $this->render("front/form/login.html.twig", [
            'lastIdent' => $lastIdent,
            'error' => $error
        ]);
    }
}