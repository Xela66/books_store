<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/admin/home' => [[['_route' => 'dashboard', '_controller' => 'App\\Controller\\AdminController::homeAdmin'], null, ['GET' => 0], null, false, false, null]],
        '/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\AdminController::logout'], null, null, null, false, false, null]],
        '/admin/author' => [[['_route' => 'app_author', '_controller' => 'App\\Controller\\AuthorController::listAuthor'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\HomeController::bienvenue'], null, ['GET' => 0], null, false, false, null]],
        '/listeArticles' => [[['_route' => 'app_list', '_controller' => 'App\\Controller\\HomeController::listArticles'], null, ['GET' => 0], null, false, false, null]],
        '/books' => [[['_route' => 'app_books', '_controller' => 'App\\Controller\\HomeController::bookList'], null, ['GET' => 0], null, false, false, null]],
        '/bookDateFilter' => [[['_route' => 'dateFiltered', '_controller' => 'App\\Controller\\HomeController::listBookFilter'], null, ['POST' => 0], null, false, false, null]],
        '/latestBooks' => [[['_route' => 'latest', '_controller' => 'App\\Controller\\HomeController::latestBooks'], null, ['GET' => 0], null, false, false, null]],
        '/addbook' => [[['_route' => 'addBook', '_controller' => 'App\\Controller\\HomeController::addBook'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'login', '_controller' => 'App\\Controller\\HomeController::login'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/admin/(?'
                    .'|addAuthor(?:/(\\d+))?(*:37)'
                    .'|delAut/(\\d+)(*:56)'
                .')'
                .'|/page/([^/]++)(*:78)'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:116)'
                    .'|wdt/([^/]++)(*:136)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:182)'
                            .'|router(*:196)'
                            .'|exception(?'
                                .'|(*:216)'
                                .'|\\.css(*:229)'
                            .')'
                        .')'
                        .'|(*:239)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        37 => [[['_route' => 'newAuthor', 'id' => -1, '_controller' => 'App\\Controller\\AuthorController::addAuthor'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        56 => [[['_route' => 'delAuteur', '_controller' => 'App\\Controller\\AuthorController::deleteAuthor'], ['id'], ['POST' => 0], null, false, true, null]],
        78 => [[['_route' => 'app_page', '_controller' => 'App\\Controller\\HomeController::page'], ['numPage'], ['GET' => 0], null, false, true, null]],
        116 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        136 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        182 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        196 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        216 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        229 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        239 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
