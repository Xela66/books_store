<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/template_part/_booksTemplate.html.twig */
class __TwigTemplate_5821ec426b316ad1b26287842c043ac8492c9c100ed68f53f652b354536b8baa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/template_part/_booksTemplate.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/template_part/_booksTemplate.html.twig"));

        // line 1
        echo "<div class=\"row d-flex flex-row justify-content-between\">
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["books"]) || array_key_exists("books", $context) ? $context["books"] : (function () { throw new RuntimeError('Variable "books" does not exist.', 2, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["book"]) {
            // line 3
            echo "        <div class=\"card mb-2 d-inline-block\" style=\"width: 30%\">
            <div class=\"card-body\">
                <h4 class=\"card-title\">";
            // line 5
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", [], "any", false, false, false, 5), "html", null, true);
            echo "</h4>
                <div class=\"card-text\">
                    <p>Auteur :
                        ";
            // line 8
            $context["i"] = 1;
            // line 9
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["book"], "author", [], "any", false, false, false, 9));
            foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
                // line 10
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "fullName", [], "any", false, false, false, 10), "html", null, true);
                echo " (né le : ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "birthFormat", [], "any", false, false, false, 10), "html", null, true);
                echo " )
                            ";
                // line 11
                if ((-1 === twig_compare((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 11, $this->source); })()), twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "author", [], "any", false, false, false, 11))))) {
                    // line 12
                    echo "                                ,
                            ";
                }
                // line 14
                echo "                            ";
                $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 14, $this->source); })()) + 1);
                // line 15
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "                    </p>
                    <p>ISBN : ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "isbn", [], "any", false, false, false, 17), "html", null, true);
            echo "</p>
                    <p>Genre : ";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["book"], "genre", [], "any", false, false, false, 18), "label", [], "any", false, false, false, 18), "html", null, true);
            echo "</p>
                    <p>Categorie de publication : ";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "categoriesNames", [], "any", false, false, false, 19), "html", null, true);
            echo "</p>
                    <p>Résumé : ";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ExcerptTextExtension']->formatExcerpt(twig_get_attribute($this->env, $this->source, $context["book"], "synopsis", [], "any", false, false, false, 20)), "html", null, true);
            echo "</p>
                </div>
            </div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['book'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/template_part/_booksTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 25,  104 => 20,  100 => 19,  96 => 18,  92 => 17,  89 => 16,  83 => 15,  80 => 14,  76 => 12,  74 => 11,  67 => 10,  62 => 9,  60 => 8,  54 => 5,  50 => 3,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row d-flex flex-row justify-content-between\">
    {% for book in books %}
        <div class=\"card mb-2 d-inline-block\" style=\"width: 30%\">
            <div class=\"card-body\">
                <h4 class=\"card-title\">{{ book.title }}</h4>
                <div class=\"card-text\">
                    <p>Auteur :
                        {% set i = 1 %}
                        {% for author in book.author %}
                            {{ author.fullName }} (né le : {{ author.birthFormat }} )
                            {% if i < book.author|length %}
                                ,
                            {% endif %}
                            {% set i = i + 1 %}
                        {% endfor %}
                    </p>
                    <p>ISBN : {{ book.isbn }}</p>
                    <p>Genre : {{ book.genre.label }}</p>
                    <p>Categorie de publication : {{ book.categoriesNames }}</p>
                    <p>Résumé : {{ book.synopsis | excerpt }}</p>
                </div>
            </div>
        </div>
    {% endfor %}
</div>", "front/template_part/_booksTemplate.html.twig", "/app/templates/front/template_part/_booksTemplate.html.twig");
    }
}
