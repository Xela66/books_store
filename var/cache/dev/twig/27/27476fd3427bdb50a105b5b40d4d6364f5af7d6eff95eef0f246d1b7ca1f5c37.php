<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* author/listAuthor.html.twig */
class __TwigTemplate_e84c259babe4d5d0a58e14fc2f38fe7087c449b7126eb1a91a0e48c657b4b09e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "author/listAuthor.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "author/listAuthor.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "author/listAuthor.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Listes des auteurs";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h2 class=\"container\">
        <span class=\"float-end\">
            <a href=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("newAuthor");
        echo "\" class=\"btn btn-success btn-sm\">Ajout</a>
        </span>
        <span>
            <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\" class=\"btn btn-info btn-md\">Accueil</a>
        </span>
        La page des auteurs</h2>
    <div class=\"container\">
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 15, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 16
            echo "            <div class=\"row d-flex flex-row justify-content-center mb-2\">
                <div class=\"alert alert-success\">";
            // line 17
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        <ul class=\"list-group\">
            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["authors"]) || array_key_exists("authors", $context) ? $context["authors"] : (function () { throw new RuntimeError('Variable "authors" does not exist.', 21, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
            // line 22
            echo "                <li class=\"list-group-item\">
                    <span class=\"float-end d-flex flex-row justify-content-end\">
                        ";
            // line 25
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("newAuthor", ["id" => twig_get_attribute($this->env, $this->source, $context["author"], "id", [], "any", false, false, false, 25)]), "html", null, true);
            echo "\"
                           class=\"btn btn-outline-warning btn-sm\">
                            <i class=\"fa-solid fa-pen-to-square\"></i>
                        </a>
                        ";
            // line 30
            echo "                        <form action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delAuteur", ["id" => twig_get_attribute($this->env, $this->source, $context["author"], "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo "\" class=\"ms-2\"
                              method=\"post\" onsubmit=\"return confirm('Etes-vous certain ?')\">
                            <input type=\"hidden\" name=\"_token\" value=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["author"], "id", [], "any", false, false, false, 32))), "html", null, true);
            echo "\">
                            <button type=\"submit\" class=\"btn btn-outline-danger btn-sm\">
                                <i class=\"fa-solid fa-trash\"></i>
                            </button>
                        </form>
                    </span>
                    <img src=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["author"], "imageFile"), "html", null, true);
            echo "\"
                         width=\"75px\">
                    ";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "fullName", [], "any", false, false, false, 40), "html", null, true);
            echo "
                </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        </ul>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "author/listAuthor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 43,  160 => 40,  155 => 38,  146 => 32,  140 => 30,  132 => 25,  128 => 22,  124 => 21,  121 => 20,  112 => 17,  109 => 16,  105 => 15,  98 => 11,  92 => 8,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block title %}Listes des auteurs{% endblock %}

{% block body %}
    <h2 class=\"container\">
        <span class=\"float-end\">
            <a href=\"{{ path(\"newAuthor\") }}\" class=\"btn btn-success btn-sm\">Ajout</a>
        </span>
        <span>
            <a href=\"{{ path(\"app_home\") }}\" class=\"btn btn-info btn-md\">Accueil</a>
        </span>
        La page des auteurs</h2>
    <div class=\"container\">
        {% for message in app.flashes('success') %}
            <div class=\"row d-flex flex-row justify-content-center mb-2\">
                <div class=\"alert alert-success\">{{ message }}</div>
            </div>
        {% endfor %}
        <ul class=\"list-group\">
            {% for author in authors %}
                <li class=\"list-group-item\">
                    <span class=\"float-end d-flex flex-row justify-content-end\">
                        {#   lien édition auteur  #}
                        <a href=\"{{ path(\"newAuthor\", {id: author.id}) }}\"
                           class=\"btn btn-outline-warning btn-sm\">
                            <i class=\"fa-solid fa-pen-to-square\"></i>
                        </a>
                        {#  lien de suppresion d'auteur  #}
                        <form action=\"{{ path('delAuteur', {id: author.id}) }}\" class=\"ms-2\"
                              method=\"post\" onsubmit=\"return confirm('Etes-vous certain ?')\">
                            <input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('delete'~author.id) }}\">
                            <button type=\"submit\" class=\"btn btn-outline-danger btn-sm\">
                                <i class=\"fa-solid fa-trash\"></i>
                            </button>
                        </form>
                    </span>
                    <img src=\"{{ vich_uploader_asset(author, 'imageFile') }}\"
                         width=\"75px\">
                    {{ author.fullName }}
                </li>
            {% endfor %}
        </ul>
    </div>
{% endblock %}", "author/listAuthor.html.twig", "/app/templates/author/listAuthor.html.twig");
    }
}
