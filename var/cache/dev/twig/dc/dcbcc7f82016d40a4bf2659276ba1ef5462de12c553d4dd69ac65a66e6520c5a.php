<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/books.html.twig */
class __TwigTemplate_e92daeafdf9fef9db9e1803b48432deffebfd93513b3e1b5e33193e6e09085e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/books.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/books.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/books.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " Liste livre ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h2 class=\"container\">Livres</h2>
    <div class=\"container row\">
        <div class=\"col-sm-3\">
            ";
        // line 9
        echo twig_include($this->env, $context, "front/template_part/_menu.html.twig", ["pp" => "books", "current" => 0]);
        echo "
        </div>
        <div class=\"col-sm-9\">
            <div class=\"row d-flex flex-row justify-content-between m-2\">
                <form method=\"post\" action=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("dateFiltered");
        echo "\" class=\"col-sm-3\">
                    <div class=\"\">
                        <label for=\"date\" class=\"form-label\">Date inférieur à :</label>
                        <input name=\"date\" type=\"date\" id=\"date\" class=\"form-control\"/>
                    </div>
                    <input type=\"submit\" value=\"chercher\" class=\"btn btn-sm btn-warning m-1\"/>
                </form>
                <div class=\"col-sm-3 justify-content-center\">
                    <a href=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("latest");
        echo "\" class=\"btn btn-warning btn-sm\">Les 5 derniers</a>
                </div>
                <div class=\"col-sm-3\">
                    <a href=\"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("addBook");
        echo "\" class=\"btn btn-success btn-sm float-end\">Ajouter un livre</a>
                </div>
            </div>
            ";
        // line 27
        echo twig_include($this->env, $context, "front/template_part/_booksTemplate.html.twig", ["books" => (isset($context["books"]) || array_key_exists("books", $context) ? $context["books"] : (function () { throw new RuntimeError('Variable "books" does not exist.', 27, $this->source); })())]);
        echo "
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "front/books.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 27,  117 => 24,  111 => 21,  100 => 13,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block title %} Liste livre {% endblock %}

{% block body %}
    <h2 class=\"container\">Livres</h2>
    <div class=\"container row\">
        <div class=\"col-sm-3\">
            {{ include(\"front/template_part/_menu.html.twig\", { pp:'books', current: 0}) }}
        </div>
        <div class=\"col-sm-9\">
            <div class=\"row d-flex flex-row justify-content-between m-2\">
                <form method=\"post\" action=\"{{ path('dateFiltered') }}\" class=\"col-sm-3\">
                    <div class=\"\">
                        <label for=\"date\" class=\"form-label\">Date inférieur à :</label>
                        <input name=\"date\" type=\"date\" id=\"date\" class=\"form-control\"/>
                    </div>
                    <input type=\"submit\" value=\"chercher\" class=\"btn btn-sm btn-warning m-1\"/>
                </form>
                <div class=\"col-sm-3 justify-content-center\">
                    <a href=\"{{ path('latest') }}\" class=\"btn btn-warning btn-sm\">Les 5 derniers</a>
                </div>
                <div class=\"col-sm-3\">
                    <a href=\"{{ path('addBook') }}\" class=\"btn btn-success btn-sm float-end\">Ajouter un livre</a>
                </div>
            </div>
            {{ include(\"front/template_part/_booksTemplate.html.twig\", { books: books}) }}
        </div>
    </div>
{% endblock %}
", "front/books.html.twig", "/app/templates/front/books.html.twig");
    }
}
