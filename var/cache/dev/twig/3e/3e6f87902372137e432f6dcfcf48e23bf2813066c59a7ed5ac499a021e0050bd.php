<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/template_part/_menu.html.twig */
class __TwigTemplate_e3c2ec484dd9da0cf6d263feee131a2bab6ffbb30ebcf6a9d577a7f6a89f77e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/template_part/_menu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/template_part/_menu.html.twig"));

        // line 1
        echo "<ul class=\"\">
    ";
        // line 2
        if ((0 !== twig_compare((isset($context["pp"]) || array_key_exists("pp", $context) ? $context["pp"] : (function () { throw new RuntimeError('Variable "pp" does not exist.', 2, $this->source); })()), "home"))) {
            // line 3
            echo "        <li class=\"list-unstyled\">
            <a href=\"";
            // line 4
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
            echo "\"
               class=\"btn btn-outline-primary btn-sm mb-2  d-md-block\">Accueil</a></li>
    ";
        }
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        // line 9
        echo "    ";
        // line 10
        echo "    ";
        // line 11
        echo "    ";
        // line 12
        echo "    ";
        // line 13
        echo "    ";
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        if ((0 !== twig_compare((isset($context["pp"]) || array_key_exists("pp", $context) ? $context["pp"] : (function () { throw new RuntimeError('Variable "pp" does not exist.', 15, $this->source); })()), "books"))) {
            // line 16
            echo "        <li class=\"list-unstyled\">
            <a href=\"";
            // line 17
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_books");
            echo "\"
               class=\"btn btn-outline-primary btn-sm mb-2  d-md-block\">Livres</a></li>
    ";
        }
        // line 20
        echo "    <li class=\"list-unstyled\">
        <a href=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_author");
        echo "\"
           class=\"btn btn-outline-primary btn-sm mb-2  d-md-block\">Auteurs</a></li>
</ul>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/template_part/_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 21,  85 => 20,  79 => 17,  76 => 16,  73 => 15,  71 => 14,  69 => 13,  67 => 12,  65 => 11,  63 => 10,  61 => 9,  59 => 8,  57 => 7,  51 => 4,  48 => 3,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<ul class=\"\">
    {% if pp != \"home\" %}
        <li class=\"list-unstyled\">
            <a href=\"{{ path(\"app_home\") }}\"
               class=\"btn btn-outline-primary btn-sm mb-2  d-md-block\">Accueil</a></li>
    {% endif %}
    {#    {% for num in [1,2,3,4]%} #}
    {#        {% if num != current %} #}
    {#        <li class=\"list-unstyled\"> #}
    {#            <a href=\"{{ path('app_page', {numPage: num}) }}\" #}
    {#                class=\"  d-md-block btn btn-outline-primary btn-sm mb-2\">Voir la page {{ num }}</a> #}
    {#        {% endif %} #}
    {#   </li> #}
    {#    {% endfor %} #}
    {% if pp != \"books\" %}
        <li class=\"list-unstyled\">
            <a href=\"{{ path(\"app_books\") }}\"
               class=\"btn btn-outline-primary btn-sm mb-2  d-md-block\">Livres</a></li>
    {% endif %}
    <li class=\"list-unstyled\">
        <a href=\"{{ path(\"app_author\") }}\"
           class=\"btn btn-outline-primary btn-sm mb-2  d-md-block\">Auteurs</a></li>
</ul>", "front/template_part/_menu.html.twig", "/app/templates/front/template_part/_menu.html.twig");
    }
}
