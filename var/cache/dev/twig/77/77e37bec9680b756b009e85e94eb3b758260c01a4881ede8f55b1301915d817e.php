<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/home.html.twig */
class __TwigTemplate_29b5bdd14c3beb91a03fae1fc879fc87fb464fd01a52fd4879b3308592413065 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Page Home";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <h2 class=\"container flex d-flex justify-content-center\">Bienvenue dans le Book Store</h2>
    <div class=\"container row\">
        <div class=\"col-sm-3\">
            ";
        // line 8
        echo twig_include($this->env, $context, "front/template_part/_menu.html.twig", ["pp" => "home", "current" => 0]);
        echo "
        </div>
        <div class=\"col-sm-9\">
            ";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDate($this->env, "now", "medium", "", null, "gregorian", "fr"), "html", null, true);
        echo "
            <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/books_store.jpg"), "html", null, true);
        echo "\" alt=\"une image\">
            <p>";
        // line 13
        echo $this->extensions['App\Twig\FormatLetterExtension']->formatLetter("Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Animi, assumenda consequatur deleniti deserunt dicta dolore est,
                expedita facilis inventore nihil obcaecati officiis perferendis
                placeat porro provident quia quo repellendus, voluptas?", "");
        // line 16
        echo "</p>
            <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ExcerptTextExtension']->formatExcerpt("Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Animi, assumenda consequatur deleniti deserunt dicta dolore est,
                expedita facilis inventore nihil obcaecati officiis perferendis
                placeat porro provident quia quo repellendus, voluptas?", 100, ""), "html", null, true);
        // line 20
        echo "</p>
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "front/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 20,  115 => 17,  112 => 16,  107 => 13,  103 => 12,  99 => 11,  93 => 8,  88 => 5,  78 => 4,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}Page Home{% endblock %}

{% block body %}
    <h2 class=\"container flex d-flex justify-content-center\">Bienvenue dans le Book Store</h2>
    <div class=\"container row\">
        <div class=\"col-sm-3\">
            {{ include(\"front/template_part/_menu.html.twig\", { pp:'home', current: 0}) }}
        </div>
        <div class=\"col-sm-9\">
            {{ 'now' | format_date(locale='fr') }}
            <img src=\"{{ asset(\"images/books_store.jpg\") }}\" alt=\"une image\">
            <p>{{ \"Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Animi, assumenda consequatur deleniti deserunt dicta dolore est,
                expedita facilis inventore nihil obcaecati officiis perferendis
                placeat porro provident quia quo repellendus, voluptas?\" | formatLetter(\"\") }}</p>
            <p>{{ \"Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Animi, assumenda consequatur deleniti deserunt dicta dolore est,
                expedita facilis inventore nihil obcaecati officiis perferendis
                placeat porro provident quia quo repellendus, voluptas?\" | excerpt(100, \"\") }}</p>
        </div>
    </div>


{% endblock %}", "front/home.html.twig", "/app/templates/front/home.html.twig");
    }
}
